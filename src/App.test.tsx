import React from 'react';
import { shallow } from 'enzyme';

import nextTick from './utils/next-tick';

import App from './App';

import { IEmailStatus } from './services/EmailsService';

describe('App component', () => {
  it('renders correctly', () => {
    shallow(<App />);
  });

  it('should show email statuses', () => {
    const dummyEmailStatuses: IEmailStatus[] = [
      { email: 'test@test.com', isMatch: true },
      { email: 'fake@test.com', isMatch: false },
    ];
    const wrapper = shallow(<App />);
    wrapper.setState({ emailStatuses: dummyEmailStatuses });
    expect(wrapper.find('EmailStatuses').prop('emailStatuses')).toBe(dummyEmailStatuses);
  });

  it('should show the latest queued email status', () => {
    const dummyQueuedEmailStatuses: IEmailStatus[] = [
      { email: 'test@test.com', isMatch: true },
      { email: 'fake@test.com', isMatch: false },
    ];
    const latestQueuedEmailStatus = dummyQueuedEmailStatuses[dummyQueuedEmailStatuses.length - 1];
    const wrapper = shallow<App>(<App />);
    wrapper.setState({ queuedEmailStatuses: dummyQueuedEmailStatuses });
    const latestEmail = wrapper.find('[data-test="latest-queued-email-status"]');
    expect(latestEmail.exists()).toBeTruthy();
    expect(latestEmail.find('EmailStatus').prop('emailStatus')).toBe(latestQueuedEmailStatus);
  });

  it('should add a new email to the queued email statuses', async () => {
    const wrapper = shallow<App>(<App />);
    // @todo Don't love this solution. How could this be better typed with the <EmailInput /> props?
    const onSubmit: any = wrapper.find('EmailInput').prop('onSubmit');
    if (onSubmit) { 
      onSubmit('test@test.com');
    }

    await nextTick();
    wrapper.update();

    expect(wrapper.state().queuedEmailStatuses.length).toBe(1);
  });

  it('should update the email statuses list on scheduler tick', () => {
    const dummyQueuedEmailStatuses: IEmailStatus[] = [
      { email: 'test@test.com', isMatch: true },
    ];
    const wrapper = shallow<App>(<App />);
    wrapper.setState({ queuedEmailStatuses: dummyQueuedEmailStatuses });
    expect(wrapper.state().emailStatuses.length).toBe(0);
    const onTick: any = wrapper.find('EmailCheckScheduler').prop('onTick');
    if (onTick) { 
      onTick();
    }
    expect(wrapper.state().emailStatuses.length).toBe(1);
  });

  it('should clear the queued email statuses on scheduler tick', () => {
    const dummyQueuedEmailStatuses: IEmailStatus[] = [
      { email: 'test@test.com', isMatch: true },
    ];
    const wrapper = shallow<App>(<App />);
    wrapper.setState({ queuedEmailStatuses: dummyQueuedEmailStatuses });
    expect(wrapper.state().queuedEmailStatuses.length).toBe(1);
    const onTick: any = wrapper.find('EmailCheckScheduler').prop('onTick');
    if (onTick) { 
      onTick();
    }
    expect(wrapper.state().queuedEmailStatuses.length).toBe(0);
  });

  it('should de-dupe email statuses on scheduler tick', () => {
    const dummyEmailStatuses: IEmailStatus[] = [
      { email: 'test@test.com', isMatch: true },
    ];
    const dummyQueuedEmailStatuses: IEmailStatus[] = [
      // Duplicate, shouldn't be pushed onto emailStatuses.
      { email: 'test@test.com', isMatch: true },
      { email: 'new@test.com', isMatch: true },
    ];
    const wrapper = shallow<App>(<App />);
    wrapper.setState({ 
      emailStatuses: dummyEmailStatuses,
      queuedEmailStatuses: dummyQueuedEmailStatuses,
    });
    const onTick: any = wrapper.find('EmailCheckScheduler').prop('onTick');
    if (onTick) { 
      onTick();
    }
    expect(wrapper.state().emailStatuses.length).toBe(2);
  });
});
