import React, { Component } from 'react';
import { uniqBy } from 'lodash';

import './App.css';

import { EmailsService, IEmailStatus } from './services/EmailsService';

import { EmailCheckScheduler } from './components/EmailCheckScheduler'; 
import { EmailInput } from './components/EmailInput';
import { EmailStatus } from './components/EmailStatus';
import { EmailStatuses } from './components/EmailStatuses';

export interface AppProps {};

export interface AppState {
  emailStatuses: IEmailStatus[],
  queuedEmailStatuses: IEmailStatus[],
};

class App extends Component<AppProps, AppState> {
  state: AppState = {
    emailStatuses: [],
    queuedEmailStatuses: [],
  }

  private emailsService: EmailsService;

  constructor (props: AppProps) {
    super(props);

    this.emailsService = new EmailsService();
  }

  async checkAndProcessEmail(email: string): Promise<IEmailStatus | null> {
    try {
      const isMatch = await this.emailsService.checkEmailStatus(email);
      return { email, isMatch };
    } catch (error) {
      console.error(`Fatal error checking email status:`, error);
      return null;
    }
  }

  handleNewEmail = async (email: string): Promise<any> => {
    const processedEmail = await this.checkAndProcessEmail(email);

    if (!processedEmail) {
      return;
    }

    const newQueuedEmailStatuses: IEmailStatus[] = [
      ...this.state.queuedEmailStatuses, 
      processedEmail,
    ];

    this.setState({ queuedEmailStatuses: newQueuedEmailStatuses });
  }

  handleSchedulerTick = (): void => {
    const newEmailStatuses: IEmailStatus[] = [
      ...this.state.emailStatuses, 
      ...this.state.queuedEmailStatuses,
    ];
    // We're happy to display immediate status of duplicated email addresses, but we don't want them in our final
    // email statuses list.
    const uniqueEmailStatuses = uniqBy(newEmailStatuses, 'email');
    this.setState({ emailStatuses: uniqueEmailStatuses, queuedEmailStatuses: [] });
  }

  render() {
    const latestQueuedEmailStatus = this.state.queuedEmailStatuses[this.state.queuedEmailStatuses.length - 1];

    return (
      <div className="App">
        <section className="hero">
          <div className="hero-body">
            <div className="container is-fluid">
              <h1 className="title">
                Welcome to my Optizmo coding test.
              </h1>
              <h2 className="subtitle">
                Please enter an email address to get started.
              </h2>

              <div className="App__form">
                <EmailInput onSubmit={this.handleNewEmail} />
                {
                  latestQueuedEmailStatus
                  ? (
                    <div data-test="latest-queued-email-status" className="App__latest-status">
                      <EmailStatus emailStatus={latestQueuedEmailStatus} />
                    </div>
                  )
                  : null
                }
              </div>

              <h4 className="title is-4">Results</h4>
              <div className="App__scheduler">
                <EmailCheckScheduler onTick={this.handleSchedulerTick} />
              </div>
              {
                this.state.emailStatuses.length
                ? <EmailStatuses emailStatuses={this.state.emailStatuses} />
                : <p><em>No email addresses tested.</em></p>
              }
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;