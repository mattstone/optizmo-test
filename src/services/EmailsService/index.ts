import GLOBAL_EMAILS from './emails.json';

export interface IEmailStatus {
  email: string;
  isMatch: boolean;
}

/**
 * Service that mocks requests related to email checking.
 */
export class EmailsService {
  DATASTORE = {
    emails: GLOBAL_EMAILS,
  }

  checkEmailStatus (email: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        const emailStatus = this.DATASTORE.emails.includes(email);
        resolve(emailStatus);
      } catch (error) {
        reject(error);
      }
    });
  }
}