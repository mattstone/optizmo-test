import { EmailsService } from './index';

describe('Emails service', () => {
  describe('Check email status', () => {
    it('should resolve successfully when an email address status matches', () => {
      const service = new EmailsService();
      service.DATASTORE.emails = ['test@test.com'];
      expect.assertions(1);
      return expect(service.checkEmailStatus('test@test.com')).resolves.toBeTruthy();
    });

    it('should resolve unsuccessfully when an email address status does not match', () => {
      const service = new EmailsService();
      service.DATASTORE.emails = ['test@test.com'];
      expect.assertions(1);
      return expect(service.checkEmailStatus('uhoh@test.com')).resolves.toBeFalsy();
    });
  });
});