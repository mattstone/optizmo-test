import React, { Component } from 'react';

import './CountdownVisualiser.css';

export interface CountdownVisualiserProps {
  duration: number;
  playing: boolean;
};

export interface CountdownVisualiserState {
  progress: number;
};


export class CountdownVisualiser extends Component<CountdownVisualiserProps, CountdownVisualiserState> {
  static defaultProps = {
    playing: true,
  }

  state: CountdownVisualiserState = {
    progress: 0, 
  }

  // Timer ID
  private timer: number = 0;
  private timerStartMS = 0;
  private timerElapsedMS = 0;

  componentDidMount() {
    this.reset();
  }

  componentDidUpdate(prevProps: CountdownVisualiserProps, prevState: CountdownVisualiserState) {
    if (!prevProps.playing && this.props.playing) {
      // If we're restarting the visualiser, update the start time to match the previously progressed duration.
      this.timerStartMS = (new Date()).getTime() - this.timerElapsedMS;
      this.tick();
    }
  }

  tick(): void {
    window.requestAnimationFrame(() => {
      if (!this.props.playing) {
        return;
      }

      // Determine how long the current countdown has progressed, in ms then percent.
      this.timerElapsedMS = ((new Date()).getTime() - this.timerStartMS);
      const newProgress = (this.timerElapsedMS / this.props.duration) * 100;

      if (newProgress >= 100) {
        this.reset();
      }

      this.setState({ progress: newProgress});

      // Schedule the next animation tick.
      this.tick();
    });
  }

  reset(): void {
    this.timerStartMS = (new Date()).getTime();
    this.tick();
  }

  render() {
    return (
      <div className="CountdownVisualiser">
        <progress 
          value={this.state.progress} 
          max="100"
          data-test="progress"
          className="CountdownVisualiser__progress progress" 
        >
          {this.state.progress}%
        </progress>
      </div>
    );
  }
}
