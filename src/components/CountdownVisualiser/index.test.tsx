import React from 'react';
import { shallow } from 'enzyme';

import { CountdownVisualiser } from './index';

jest.useFakeTimers();

describe('CountdownVisualiser component', () => {
  beforeEach(() => {
    // https://github.com/facebook/jest/issues/5147#issuecomment-353274996
    // @todo Find more accurate solution for simulating RAF.
    jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb =>
      window.setTimeout(() => cb(1), 50)
    );
  });

  afterEach(() => {
    // =)
    (window.requestAnimationFrame as any).mockRestore();
  });

  it('renders correctly', () => {
    shallow(<CountdownVisualiser duration={1000} />);
  });

  it('should progress a countdown visualiser over time', () => {
    const wrapper = shallow<CountdownVisualiser>(<CountdownVisualiser duration={1000} playing={true} />);
    expect(wrapper.find('[data-test="progress"]').props().value).toBe(0);

    jest.advanceTimersByTime(1000);

    // Check the visualiser percentage has increased.
    // !! NOTE: The value here is not currently accurate due to RAF simulation. We just want to know that it has
    // progressed any amount.
    expect(wrapper.find('[data-test="progress"]').props().value).toBeGreaterThan(0);
  });

  it('should reset visualisation after reaching the duration', () => {
    // @todo Implement.
  });

  it('should pause visualisation', () => {
    // @todo Implement.
  });

  it('should restart visualisation', () => {
    // @todo Implement.
  });
});