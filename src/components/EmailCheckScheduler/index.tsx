import React, { Component } from 'react';
import classNames from 'classnames';

import './EmailCheckScheduler.css';

import { CountdownVisualiser } from '../CountdownVisualiser';

export interface EmailCheckSchedulerProps {
  duration: number;
  onTick: () => void;
};

export interface EmailCheckSchedulerState {
  started: boolean;
};

export class EmailCheckScheduler extends Component<EmailCheckSchedulerProps, EmailCheckSchedulerState> {
  static defaultProps = {
    duration: 10000, // 10 seconds
    onTick: () => {},
  }

  state: EmailCheckSchedulerState = {
    started: true,
  }

  // Timer ID
  private timer: number = 0;
  private timerStartMS = 0;
  private timerRemainingMS = 0;

  componentDidMount() {
    this.start();
  }

  handleClickTimerControl = (event: React.MouseEvent<HTMLElement>): void => {
    this.state.started ? this.stop() : this.start();
  }

  start(): void {
    this.setState({ started: true });
    this.tick();
  }

  stop(): void {
    this.setState({ started: false });

    // Store remaining timer duration in case we restart the timer later (unpause).
    this.timerRemainingMS = this.props.duration - ((new Date()).getTime() - this.timerStartMS);

    window.clearTimeout(this.timer);
    this.timer = 0;
  }

  tick(): void {
    this.timer = window.setTimeout(() => {
      this.start();
      this.props.onTick();
    }, this.timerRemainingMS || this.props.duration);

    // Store current start time so we can calculate remaining timer duration later.
    this.timerStartMS = (new Date()).getTime();
    this.timerRemainingMS = 0;
  }

  render() {
    const { duration } = this.props;

    return (
      <div className="EmailCheckScheduler">
        <div className="EmailCheckScheduler__visualiser">
          <CountdownVisualiser 
            duration={duration} 
            playing={this.state.started} 
          />
        </div>
        <div className="EmailCheckScheduler__control">
          <button 
            type="button" 
            title={this.state.started ? 'Pause' : 'Resume'} 
            data-test="control"
            onClick={this.handleClickTimerControl} 
            className="button"
          >
            <span className="icon is-small">
              <i className={classNames('fas', {
                'fa-play': !this.state.started,
                'fa-pause': this.state.started,
              })}></i>
            </span>
          </button>
        </div>
      </div>
    );
  }
}