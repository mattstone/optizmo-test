import React from 'react';
import { shallow } from 'enzyme';

import { EmailCheckScheduler, EmailCheckSchedulerProps } from './index';

jest.useFakeTimers();

describe('EmailCheckScheduler component', () => {
  it('renders correctly', () => {
    shallow(<EmailCheckScheduler />);
  });

  it('should render a progress visualiser', () => {
    const wrapper = shallow(<EmailCheckScheduler duration={1000} />);
    const visualiser = wrapper.find('CountdownVisualiser');
    expect(visualiser.prop('duration')).toBe(1000);
  });

  it('should pause the scheduler', () => {
    const props: EmailCheckSchedulerProps = {
      duration: 100,
      onTick: jest.fn()
    };
    const wrapper = shallow(<EmailCheckScheduler {...props} />);
    jest.advanceTimersByTime(100);
    wrapper.find('[data-test="control"]').simulate('click');
    jest.advanceTimersByTime(400);

    // Callback should only have been called once, it was paused after one tick.
    expect(props.onTick).toHaveBeenCalledTimes(1);
    expect(wrapper.find('[data-test="control"]').prop('title')).toBe('Resume');
    expect(wrapper.find('CountdownVisualiser').prop('playing')).toBeFalsy();
  });
  it('should restart the scheduler', () => {
    const props: EmailCheckSchedulerProps = {
      duration: 100,
      onTick: jest.fn()
    };
    const wrapper = shallow(<EmailCheckScheduler {...props} />);
    wrapper.find('[data-test="control"]').simulate('click');
    jest.advanceTimersByTime(400);
    expect(props.onTick).toHaveBeenCalledTimes(0);
    wrapper.find('[data-test="control"]').simulate('click');
    jest.advanceTimersByTime(100);

    // Callback should only have been called once, it has been resumed for one tick.
    expect(props.onTick).toHaveBeenCalledTimes(1);
    expect(wrapper.find('[data-test="control"]').prop('title')).toBe('Pause');
    expect(wrapper.find('CountdownVisualiser').prop('playing')).toBeTruthy();
  });

  it('should call `onTick` when the the scheduler completes', () => {
    const props: EmailCheckSchedulerProps = {
      duration: 100,
      onTick: jest.fn()
    };
    shallow(<EmailCheckScheduler {...props} />);
    jest.advanceTimersByTime(500);
    expect(props.onTick).toHaveBeenCalledTimes(5);
  });
});