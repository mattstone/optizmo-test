import React, { Component } from 'react';

import { EmailStatus } from '../EmailStatus';

import { IEmailStatus } from '../../services/EmailsService';

export interface EmailStatusesProps {
  emailStatuses: IEmailStatus[];
};

export interface EmailStatusesState {
  email: string;
};

export class EmailStatuses extends Component<EmailStatusesProps, EmailStatusesState> {
  static defaultProps = {
    emailStatuses: [],
  }

  sortEmailStatuses(emailStatuses: IEmailStatus[]): IEmailStatus[] {
    return emailStatuses.sort((a, b) => {
      // First sort by matched status.
      if (a.isMatch !== b.isMatch) {
        return a.isMatch ? -1 : 1;
      }

      // Then alphanumerically by email.
      return a.email.localeCompare(b.email);
    });
  }

  render() {
    const sortedEmailStatuses = this.sortEmailStatuses(this.props.emailStatuses);

    return (
      <div className="EmailStatuses list">
        {sortedEmailStatuses.map((emailStatus) => 
          <div key={emailStatus.email} className="list-item">
            <EmailStatus emailStatus={emailStatus} />
          </div>
        )}
      </div>
    );
  }
}
