import React from 'react';
import { shallow } from 'enzyme';

import { EmailStatuses } from './index';

import { IEmailStatus } from '../../services/EmailsService';

const EMAIL_STATUSES: IEmailStatus[] = [
  { email: 'test@test.com', isMatch: false },
  { email: 'matt@mattstone.com.au', isMatch: true },
  { email: 'bigben@clock.com', isMatch: false },
  { email: 'abc@test.com', isMatch: true },
];

describe('EmailStatuses component', () => {
  it('renders correctly', () => {
    shallow(<EmailStatuses />);
  });

  it('renders a list of email statuses', () => {
    const wrapper = shallow<EmailStatuses>(<EmailStatuses emailStatuses={EMAIL_STATUSES} />);
    expect(wrapper.find('EmailStatus').length).toBe(4);
  });

  it('sorts email statuses correctly', () => {
    const wrapper = shallow<EmailStatuses>(<EmailStatuses emailStatuses={EMAIL_STATUSES} />);
    expect(wrapper.instance().sortEmailStatuses(EMAIL_STATUSES)).toEqual([
      // Sort first by `isMatch` status, then alphanumerically.
      { email: 'abc@test.com', isMatch: true },
      { email: 'matt@mattstone.com.au', isMatch: true },
      { email: 'bigben@clock.com', isMatch: false },
      { email: 'test@test.com', isMatch: false },
    ]);
  });
});