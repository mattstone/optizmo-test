import React, { Component } from 'react';
import classNames from 'classnames';

import './EmailStatus.css';

import { IEmailStatus } from '../../services/EmailsService';

export interface EmailStatusProps {
  emailStatus: IEmailStatus;
};

export interface EmailStatusState {};

export class EmailStatus extends Component<EmailStatusProps, EmailStatusState> {
  render() {
    const { emailStatus } = this.props;

    return (
      <div title={emailStatus.isMatch ? 'Match' : 'No match'} className="EmailStatus">
        <span className={classNames('EmailStatus__icon icon is-small', {
          'has-text-success': emailStatus.isMatch,
          'has-text-warning': !emailStatus.isMatch,
        })}>
          <i 
            data-test="icon"
            className={classNames('fas', {
              'fa-check-circle': emailStatus.isMatch,
              'fa-times-circle': !emailStatus.isMatch,
            })}
          ></i>
        </span>
        <span className="EmailStatus__title">{emailStatus.email}</span>
      </div>
    );
  }
}
