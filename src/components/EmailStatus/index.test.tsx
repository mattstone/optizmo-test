import React from 'react';
import { shallow } from 'enzyme';

import { EmailStatus } from './index';

import { IEmailStatus } from '../../services/EmailsService';

describe('EmailStatus component', () => {
  it('renders correctly', () => {
    const dummyEmailStatus: IEmailStatus = { email: 'test@test.com', isMatch: true };
    shallow<EmailStatus>(<EmailStatus emailStatus={dummyEmailStatus} />);
  });

  it('shows an "match" icon for a matched email status', () => {
    const dummyEmailStatus: IEmailStatus = { email: 'test@test.com', isMatch: true };
    const wrapper = shallow<EmailStatus>(<EmailStatus emailStatus={dummyEmailStatus} />);
    expect(wrapper.find('[data-test="icon"]').hasClass('fa-check-circle')).toBeTruthy();
  });
    it('shows an "no match" icon for an unmatched email status', () => {
    const dummyEmailStatus: IEmailStatus = { email: 'test@test.com', isMatch: false };
    const wrapper = shallow<EmailStatus>(<EmailStatus emailStatus={dummyEmailStatus} />);
    expect(wrapper.find('[data-test="icon"]').hasClass('fa-times-circle')).toBeTruthy();
  });
});