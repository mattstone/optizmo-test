import React, { Component } from 'react';

import './EmailInput.css';

export interface EmailInputProps {
  onSubmit: (email: string) => void;
};

export interface EmailInputState {
  email: string;
};

export class EmailInput extends Component<EmailInputProps, EmailInputState> {
  static defaultProps = {
    onSubmit: () => {},
  }

  state: EmailInputState = {
    email: '',
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ email: event.currentTarget.value });
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();

    this.props.onSubmit(this.state.email);

    // Clear input, ready to accept another email address.
    this.setState({ email: '' });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="EmailInput">
        <div className="field has-addons">
          <div className="control has-icons-left is-expanded">
            <input 
              type="email" 
              name="email"
              value={this.state.email} 
              onChange={this.handleChange} 
              placeholder="Email address to test" 
              required 
              className="input" 
            />
            <span className="icon is-small is-left">
              <i className="fas fa-envelope"></i>
            </span>
          </div>
          <div className="control">
            <input type="submit" value="Test" className="button is-primary" />
          </div>
        </div>
      </form>
    );
  }
}