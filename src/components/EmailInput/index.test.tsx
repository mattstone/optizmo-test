import React from 'react';
import { shallow } from 'enzyme';

import { EmailInput, EmailInputProps } from './index';

describe('EmailInput component', () => {
  it('renders correctly', () => {
    shallow(<EmailInput />);
  });

  it('should call `onSubmit` when the form is submitted', () => {
    const props: EmailInputProps = {
      onSubmit: jest.fn()
    };
    const wrapper = shallow<EmailInput>(<EmailInput {...props} />);
    const form = wrapper.find('form').first();
    wrapper.find('input[name="email"]').simulate('change', { currentTarget: { value: 'test@test.com' } });
    form.simulate('submit', { preventDefault() {} });
    expect(props.onSubmit).toHaveBeenCalledWith('test@test.com');
  });

  it('should clear input after form has been submitted', () => {
    const wrapper = shallow<EmailInput>(<EmailInput />);
    const form = wrapper.find('form');
    wrapper.find('input[name="email"]').simulate('change', { currentTarget: { value: 'test@test.com' } });
    expect(wrapper.find('input[name="email"]').prop('value')).toBe('test@test.com');
    form.simulate('submit', { preventDefault() {} });
    expect(wrapper.find('input[name="email"]').prop('value')).toBe('');
  });
});